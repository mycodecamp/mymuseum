package com.inbra.aqua;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;


import com.inbra.aqua.animation.BounceInterpolator;
import com.inbra.aqua.watson.AssistantDelegator;
import com.inbra.aqua.watson.TextSpeechDelegator;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;


@Deprecated
public class ChatActivity extends FragmentActivity {

    public static final String EXTRA_IMAGE_URL = "detailImageUrl";
    public static final String IMAGE_TRANSITION_NAME = "transitionImage";
    public static final String NAME_TRANSITION_NAME = "transitionName";

    private ImageView imageView;
    private ImageView watsonView;
    private TextView nameView;
    private List<Message> messages;
    private RecyclerView recyclerViewMessages;
    private Dialog progressDialog;
    private FloatingActionButton fab_send;

    //private  Animation buttonAnimation;
    private Animation watsonAnimation;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        // ref to views
        {
            imageView = (ImageView) findViewById(R.id.fish_image);
            nameView = (TextView) findViewById(R.id.fish_name);
            // fab_send = findViewById(R.id.fab_btn_send);

        }
        // setting animations
        {
            watsonAnimation = AnimationUtils.loadAnimation(this, R.anim.watson_animation);
            watsonAnimation.setRepeatCount(Animation.INFINITE);

            BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
            watsonAnimation.setInterpolator(interpolator);

        }

        // progress dialog with watson logo
        {
            progressDialog = new Dialog(this);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.setContentView(R.layout.watson_layout);
            watsonView = (ImageView) progressDialog.findViewById(R.id.watson_icon);
        }


        // init message list
        {
            recyclerViewMessages = findViewById(R.id.rv_messages);

            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewMessages.setLayoutManager(llm);

            test();

            recyclerViewMessages.setAdapter(new MessagesAdapter(messages));
        }


        // translucent activity
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }


        // set image on view
        {
            String imageUrl = getIntent().getStringExtra(EXTRA_IMAGE_URL);
            ImageLoader.getInstance().displayImage(imageUrl, imageView);
        }

        // transition view and name views
        {
            ViewCompat.setTransitionName(imageView, IMAGE_TRANSITION_NAME);
            ViewCompat.setTransitionName(nameView, NAME_TRANSITION_NAME);
        }


        // keyboard pane
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        // button events
        {
            fab_send.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        AsyncTask<Void, Void, Void> s = new AsyncTask<Void, Void, Void>() {
                            @Override
                            protected Void doInBackground(Void... voids) {
                                try {
                                    // AssistantDelegator delegator = new AssistantDelegator();
                                    // delegator.init();

                                    TextSpeechDelegator text = new TextSpeechDelegator();

                                    text.init();
                                } catch (Exception e) {
                                    Log.i("aqua", e.getLocalizedMessage(), e);
                                }
                                return null;
                            }
                        };
                        s.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        progressDialog.show();
                        watsonView.startAnimation(watsonAnimation);

                        return true;
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        watsonAnimation.cancel();
                        return true;
                    }
                    return false;
                }
            });
        }

    }

    public void test() {
        // test

        this.messages = new ArrayList<>();
        {
            Message m = new Message();
            m.message = "Ola Mundo";
            m.type = 0;
            this.messages.add(m);
        }
        {
            Message m = new Message();
            m.message = "texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto texto";
            m.type = 1;
            this.messages.add(m);
        }


    }

    // message adapter
    public class MessagesAdapter extends RecyclerView.Adapter {
        private List<Message> messages;

        public MessagesAdapter(List<Message> messages) {
            this.messages = messages;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView1 = null;

            if (viewType == 0) {
                itemView1 = LayoutInflater.from(ChatActivity.this)
                        .inflate(R.layout.chat_item_sent, parent, false);
            }

            if (viewType == 1) {
                itemView1 = LayoutInflater.from(ChatActivity.this)
                        .inflate(R.layout.chat_item_rcv, parent, false);
            }
            return new MessageHolder(itemView1);
        }

        @Override
        public int getItemViewType(int position) {
            return messages.get(position).type;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            Message msg = messages.get(position);
            ((MessageHolder) holder).message_text_view.setText(msg.message);
            if (msg.type == 1) {
                ((MessageHolder) holder).sender_text_view.setText("Watson");
            }
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

    }

    public class MessageHolder extends RecyclerView.ViewHolder {

        protected TextView message_text_view;
        protected TextView sender_text_view;

        public MessageHolder(View itemView) {
            super(itemView);
            message_text_view = itemView.findViewById(R.id.message_text_view);
            sender_text_view = itemView.findViewById(R.id.sender_text_view);
        }


    }

    // message data
    public class Message {
        public String message;
        public int type;
    }
}