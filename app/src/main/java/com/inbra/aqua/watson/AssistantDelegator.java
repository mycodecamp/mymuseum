package com.inbra.aqua.watson;

import android.os.AsyncTask;
import android.util.Log;

import com.ibm.watson.developer_cloud.assistant.v1.Assistant;
import com.ibm.watson.developer_cloud.assistant.v1.model.Context;
import com.ibm.watson.developer_cloud.assistant.v1.model.InputData;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageOptions;
import com.ibm.watson.developer_cloud.assistant.v1.model.MessageResponse;

import java.util.List;

public class AssistantDelegator {


    private Assistant assistant;

    public MessageResponse last_response;


    public void init() {
        this.assistant = new Assistant("2018-09-12", "410f8fca-fa37-4d12-af47-7bf4eef5c623", "SKTFz5F8OINS");
        this.assistant.setEndPoint("https://gateway.watsonplatform.net/assistant/api");

    }

    public void message(final String text, final Callback callback) throws Exception {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public MessageResponse response_msg;

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Context context = new Context();

                    if (AssistantDelegator.this.last_response != null) {
                        context = AssistantDelegator.this.last_response.getContext();
                        context.setConversationId(AssistantDelegator.this.last_response.getContext().getConversationId());
                    }

                    // context.put()
                    InputData input = new InputData.Builder(text).build();
                    MessageOptions options = new MessageOptions.Builder("02ad0123-309c-4561-8748-dcd1daf368fd")
                            .input(input)
                            .context(context)
                            .build();
                    this.response_msg = assistant.message(options).execute();
                    AssistantDelegator.this.last_response = this.response_msg;
                    Log.i("aqua", response_msg.toString());
                } catch (Exception e) {
                    Log.i("aqua", e.getLocalizedMessage(), e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (this.response_msg != null) {
                    List<String> texts = response_msg.getOutput().getText();
                    for (String text : texts) {
                        callback.text(text);
                    }
                }
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public static interface Callback {
        public void text(String text);
    }
}

