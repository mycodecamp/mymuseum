package com.inbra.aqua.watson;

import android.os.AsyncTask;
import android.util.Log;

import com.ibm.watson.developer_cloud.http.HttpMediaType;
import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.RecognizeOptions;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionAlternative;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionResult;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.SpeechRecognitionResults;

import java.io.File;
import java.util.List;

public class SpeechTextDelegator {

    private SpeechToText speechService;

    public void init() {
        this.speechService = new SpeechToText("2697fc20-065f-41b4-8d2f-5f1c4be5084b", "u7MaoJ7u0ZEQ");
        this.speechService.setEndPoint("https://stream.watsonplatform.net/speech-to-text/api");
    }


    public void recognize(final File audio, final Callback callback) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            public SpeechRecognitionResults transcript;

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    RecognizeOptions options = new RecognizeOptions.Builder()
                            .audio(audio)
                            .model("pt-BR_BroadbandModel")
                            .contentType(HttpMediaType.AUDIO_WAV)
                            .build();

                    this.transcript = speechService.recognize(options).execute();
                } catch (Exception e) {
                    Log.i("aqua", e.getLocalizedMessage(), e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (transcript != null) {
                    List<SpeechRecognitionResult> results = transcript.getResults();
                    for (SpeechRecognitionResult result : results) {
                        List<SpeechRecognitionAlternative> alternatives = result.getAlternatives();
                        for (SpeechRecognitionAlternative alternative : alternatives) {
                            String text = alternative.getTranscript();
                            Log.i("aqua", "text: " + text);
                            callback.text(text);
                        }
                    }
                } else {
                    callback.error("");
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


    }

    public static interface Callback {
        public void text(String text);

        public void error(String message);
    }
}

