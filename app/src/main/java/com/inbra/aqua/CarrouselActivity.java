package com.inbra.aqua;

import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.inbra.aqua.animation.CustPagerTransformer;
import com.inbra.aqua.animation.BounceInterpolator;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class CarrouselActivity extends AppCompatActivity {


    private ViewPager viewPager;
    private FloatingActionButton fab_mic;
    private FloatingActionButton fab_send;
    private List<ImageCardFragment> fragments;
    private ImageCardFragment currentFragment;

    // test images
    private final String[] imageArray = {"assets://image1.jpg", "assets://image2.jpg", "assets://image3.jpg", "assets://image4.jpg", "assets://image5.jpg"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carrousel);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        // load imagens
        initImageLoader();

        // ref a componentes
        viewPager = (ViewPager) findViewById(R.id.viewpager_images);
        // fab_mic = findViewById(R.id.fab_btn_mic);
        // fab_send = findViewById(R.id.fab_btn_send);


        // setting buttons
        {
            final Animation buttonAnimation = AnimationUtils.loadAnimation(this, R.anim.bounce);
            BounceInterpolator interpolator = new BounceInterpolator(0.2, 20);
            buttonAnimation.setInterpolator(interpolator);

            fab_mic.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!fab_mic.isClickable()) {
                        return false;
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        fab_mic.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
                        fab_mic.startAnimation(buttonAnimation);
                        return true;
                    }

                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        fab_mic.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(android.R.color.black)));
                        return true;
                    }
                    return false;
                }
            });

            fab_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    currentFragment.gotoDetail();
                }
            });
        }

        // view pager
        {
            viewPager.setPageTransformer(false, new CustPagerTransformer(this));

            fragments = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                fragments.add(new ImageCardFragment());
            }

            viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
                @Override
                public Fragment getItem(int position) {
                    // for test, get random fragment
                    final ImageCardFragment fragment = fragments.get(position % 10);
                    fragment.bindData(imageArray[position % imageArray.length]);
                    // fragment.setActivity(CarrouselActivity.this);
                    return fragment;
                }

                @Override
                public int getCount() {
                    return 10;
                }
            });


            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(final int position) {
                    // show or hide bottom panel
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if ((position - 1) >= 0) {
                                fragments.get(position - 1).collapse();
                                fragments.get(position - 1).left();

                            }

                            currentFragment = fragments.get(position);
                            currentFragment.center();
                            currentFragment.expand();

                            if ((position + 1) < fragments.size()) {
                                fragments.get(position + 1).collapse();
                                fragments.get(position + 1).right();
                            }
                        }
                    });
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                    hideButtons();
                }
            });

            // select default page
            viewPager.post(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(1, false);
                }
            });

            viewPager.setHorizontalFadingEdgeEnabled(true);
            viewPager.setFadingEdgeLength(20);

            // margins for show 3 views on page
            viewPager.setOffscreenPageLimit(3);
            viewPager.setPageMargin(
                    getResources().getDimensionPixelOffset(R.dimen.viewpager_margin));
        }

        // hide fab buttons
        // hideButtons();

    }

    public void showButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fab_mic.setClickable(true);
                fab_mic.setAlpha(1f);

                fab_send.setClickable(true);
                fab_send.setAlpha(1f);
            }
        });
    }


    public void hideButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                fab_mic.setClickable(false);
                fab_mic.setAlpha(0.2f);

                fab_send.setClickable(false);
                fab_send.setAlpha(0.2f);
            }
        });
    }


    private void initImageLoader() {
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                this)
                .memoryCacheExtraOptions(480, 800)
                // default = device screen dimensions
                .threadPoolSize(3)
                // default
                .threadPriority(Thread.NORM_PRIORITY - 1)
                // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024).memoryCacheSizePercentage(13) // default
                .discCacheSize(50 * 1024 * 1024)
                .discCacheFileCount(100)
                .discCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(this)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs().build();


        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
    }
}
